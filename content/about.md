+++
title = "About"
+++

## <span style='text-decoration: underline;'>Tentang</span>
{{< figure class="avatar" src="/josrac.jpg" >}}

<span style='text-decoration: underline;'>Johor Scout Amateur Radio Crew</span> atau nama singkatan <span style='text-decoration: underline;'>JOSRAC</span> telah ditubuhkan pada tahun 2008. Pada peringkat awal satu jawatankuasa penaja telah dibentuk yang dipengerusikan oleh Penolong Pesuruhjaya Pengakap Negeri unit radio amatur dan ahli jawatankuasa terdiri pemimpin pengakap yang memegang lesen radio amatur wakil dari setiap daerah.

Pada tanggal 21 bulan Oktober tahun 2010, JOSRAC telah [berdaftar dengan ibu pejabat Persekutuan Pengakap Malaysia (PPM)](../josrac_sijil_pendaftaran.jpg) sebagai **Pasukan Kelana Komunikasi Negeri Johor (JOSRAC)** dengan no.pendaftaran **M.10187**.  Walaupun nama asal telah dipinda atas kehendak pihak ibu pejabat PPM tetapi singkatan JOSRAC masih dikekalkan sebagai identiti.

## <span style='text-decoration: underline;'>Tujuan penubuhan</span>
### Misi
Memberikan pendedahan dan peluang kepada warga pengakap dalam melengkapkan pengetahuan di bidang teknologi komunikasi radio.
### Visi
Menjalankan kegiatan komunikasi radio amatur melalui aktiviti kepengakapan serta meningkatkan kerjasama dengan pertubuhan-pertubuhan yang berkaitan.
### Objektif
Merancang dan melaksanakan program-program serta kemahiran yang bermanfaat demi memajukan pergerakan pengakap dalam perkembangan teknologi komunikasi radio.
