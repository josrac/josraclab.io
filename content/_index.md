## Pengenalan

{{< figure class="avatar" src="/josrac.jpg" >}}

Selamat datang ke halaman sesawang **Pasukan Kelana Komunikasi Negeri Johor (JOSRAC) - M.10187** atau juga dikenali sebagai **KELKOM** atau **Johor Scout Amateur Radio Crew**.

Kami adalah krew kelana dewasa yang bergiat aktif dalam bidang komunikasi terutamanya pada cabang <span style='text-decoration: underline;'>radio amatur</span>. Dianggotai oleh pelbagai umur dan bangsa seramai lebih dari 29 orang dari yang bergiat secara aktif dan juga secara pasif.

Tujuan laman web ini diwujudkan ialah untuk <span style='text-decoration: underline;'>berkongsi maklumat tentang aktiviti, ilmu kepengakapan dan juga pengetahuan umum</span> berkaitan bidang komunikasi.

> Sekiranya terdapat infomasi atau fakta yang silap, salah kurang tepat atau tidak dibenarkan untuk dikongsi, kami memohon maaf dan kami menerima segala teguran secara terbuka. Sila [hubungi](contact) kami melalui platform yang disediakan.

Untuk maklumat lebih lanjut mengenai JOSRAC, sila ke bahagian [tentang](about). Bagi melihat aktiviti, perkongsian dan artikel, sila ke bahagian [blog](blog). Sekiranya berminat dengan produk dan khidmat kami, sila ke bahagian [projek](projects). Untuk rekod rasmi, sila ke bahagian [nota](notes). 

Sekiranya anda berminat untuk menyertai kami, sila hubungi [cawangan](branch) (daerah) terdekat.

Sekian, Terima kasih.

## Hakcipta
Segala hasil karya dilesenkan di bawah [`CC BY-NC 4.0`](https://creativecommons.org/licenses/by-nc/4.0/deed.ms) kecuali jika [dinyatakan](https://gitlab.com/josrac/josrac.gitlab.io/-/tree/master/LICENSE) sebaliknya.