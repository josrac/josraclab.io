+++
title = "µSDX QSO across the sea between Malaysia (9M4CPJ) with Indonesia (YB7DYY)"
slug = "µSDX-qso-malaysia-indonesia"
date = "2021-04-30"
externalLink = "https://www.youtube.com/watch?v=dztOwTJgFfg"
tags = ['µSDX', 'Youtube', '9M4CPJ', '9M4CPJ', 'End Fed Half Wave', '400 Watts', 'YB7DYY', 'Indonesia', 'Malaysia', '10dB', 'SDR', '9W2ABP', 'Batu Pahat', '4 Watts' ]
categories = []
+++