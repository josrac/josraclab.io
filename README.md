# About
This is source code of [http://josrac.gitlab.io](http://josrac.gitlab.io/)

## Development
Incase you are interested to extending the functionality of this weblog. Please follow requirements and steps below:

### Requirement
- text editor
- git
- hugo

### Steps
```bash
git submodule update --remote themes/hugo-researcher-josrac
```
Note : theme are keep on github repository